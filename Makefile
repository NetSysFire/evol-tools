XSLTPROC = xsltproc

all:
	@echo "Current makes:"
	@echo "contrib      - makes contributors list for wiki and client-data"
	@echo "testxml      - performs a minimal XML test"
	@echo "wiki         - makes wiki automated databases"
	@echo "maps         - makes server-data information"
	@echo "update       - makes update and propagate web information (untested)"
	@echo "news         - propagate web information"
	@echo "translation  - fetches Transifex translations for server, and updates both."
	@echo "client       - generate weapons.xml, minimaps and daily.xml"
	@echo "cnomini      - generate client-data except minimaps"
	@echo "full         - make contrib + wiki + translation + client + update + news"

contrib:
	cd contrib_xsl ; make
	@echo "Please commit the result on wiki and clientdata."

.PHONY: testxml
testxml:
	cd testxml ; ./testxml.py silent |grep -v "Checking"

.PHONY: wiki
wiki:
	cd wiki ; ./wikigen.py
	cd wiki ; mv *.md ../../wiki/
	@cd contrib_xsl ; make wiki
	@echo "Wiki files updated, please commit the result on wiki."

.PHONY: maps
maps:
	cd hercules ; ./tmx_converter.py ../../client-data/ ../../server-data
	cd hercules ; ./convert_tmx_to_mapcache.py
	@echo "Please commit the result on serverdata."

.PHONY: update
update:
	@cd ../web ; echo "Checking for web/ folder..." # check if web directory exists
	cd update ; ./update.sh
	cd wiki ; ./elegen.py ; mv EleMonsters.html ../../web/Monsters.html
	cd web ; ./deploy.sh
	@echo "Update deployed, please move upload/ and commit result on web."
	@echo "Don't forget to make translations, client, news and wiki too!"

translation:
	@echo "Fetching server translations, ensure lang/login.txt and lang/password.txt exists..."
	cd lang/ ; ./fetch.py
	@echo "Updating server translations and preparing for pull..."
	cd lang/ ; ./updatelang.py
	@echo "Fetching client translations, ensure lang/login.txt and lang/password.txt exists..."
	cd lang_client/ ; ./fetch.py
	@echo "Updating client translations and preparing for pull..."
	cd lang_client/ ; ./createpots.sh
	@echo "Translations updated, please commit result on server-data, client-data and tools."

.PHONY: news
news:
	@cd ../web ; echo "Checking for web/ folder..." # check if web directory exists
	cd web ; ./deploy.sh
	@echo "News updated, please commit result on web."
	@cd ../gameinfo-api ; echo "Checking for gameinfo-api/ folder..." # check
	cd web ; ./gameinfo.sh

.PHONY: client
client:
	@cd client ; ./minimap-render.py all
	@cd client ; ./minimap-dyecmd.py ; ./minimap-dyecmd.sh
	cp client/minimap-override/* ../client-data/graphics/minimaps/
	@git checkout -- client/minimap-dyecmd.sh
	make cnomini

.PHONY: cnomini
cnomini:
	@cd client ; ./weapons.py
	@cd client ; ./dailylogin.py
	@cd client ; ./aurora.py
	@cd client ; ./skills.py
	@cd client ; mv weapons.tmp ../../client-data/weapons.xml
	@cd client ; mv daily.tmp ../../client-data/graphics/images/daily.xml
	@cd client ; mv aurora.tmp ../../client-data/graphics/images/aurora.xml
	@cd client ; mv skills.tmp ../../client-data/mana_skills.xml
	@cd contrib_xsl ; make about-server
	@echo "XML files and minimaps updated, please commit result on client-data."

.PHONY: full
full:
	@echo "full         - make contrib + wiki + translation + client + update + news"
	make contrib
	make wiki
	make translation
	make client
	make update
	make news

