#!/usr/bin/python

def pretnum(g):
	h="{:,}".format(int(g))
	print g+" -> "+h
	return h
	h=""
	l=0
	for char in g:
		if l % 3 == 2:
			h+=","
		l+=1
		h+=char
	print "%s -> %s" % (g, h)
	return h

a=open("exp.conf", "r")
b=open("xp.html", "w")

for c in a:
	d=c.split(',')
	i=0
	# For entry
	for e in d:
		# Output current limit
		if i == 0:
			b.write('Max Level: '+d[0]+'\n<table border=1><tr>')
		# Break line every 10 entries
		elif i % 10 == 3:
			b.write('</tr>\n<tr>')
		# Make an empty space every 100 levels
		if i % 100 == 3 and int(i/100) >= 1:
			z=0
			# loop to make prettier
			while z < 9:
				z+=1
				b.write('<td/>')
			b.write('<td/></tr>\n<tr>')
		# Do not print headers, race or type as if they were content
		if i not in [0,1,2]:
			b.write('<td><b>%d</b><br/>%s</td>' % (i-2, pretnum(e) ))
		i+=1
	# Close this table, repeat the everyline loop
	b.write('</tr></table>\n\n')
