#!/usr/bin/python
# This script file is meant to create EXP table
# It have some randomness (see the import)
# It starts at level 51 (and i is the according xp)
# It stops at level 19999, or when MAX_INT is reached.
import random
i=15290
l=31
o=open("expgroup.db", "w")

mx=255
b=False

while (l < mx):
    l+=1
    value=l/random.randint(1,5)
    #value=l+random.randint(1,5)
    bonus=random.randint(-2,12)
    fact=1+(1.0/value)
    i=int(i*fact)+bonus

    if (i >= pow(2,31)):
        if not b:
            print("Interruption at level %d: max int reached" % l)
            b=True
        i=pow(2,31)-1000+l
    o.write('%d,' % i)
    if l % 10 == 0:
        o.write('\n')

o.close()
