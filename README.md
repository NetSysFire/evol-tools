# The Monster World: Monster Wars 2 Toolset

This repository holds many tools for TMW2 project.

## Translation Related

Run `make translation` to update them on server and on transifex. Requires to have
a project maintaner status on Transifex, and having login/password txt files.

## Update Related

Server news can be found at  `update/news.txt`. Use `make update` to deploy an
update to client.

## Localhost Related

When running a localhost, you'll need to follow the instructions at [localserver/README](localserver/README)
or it won't work at all.

## Other Make Commands

Use `make` to get a list of all make commands. For example, `make wiki`.

## DyeCmd Related

Some scripts to make pallete-removal easier are available at `dyecmd/` folder, by
Jesusalva. Do not use them. (They also won't work unless you copy files to the
right place, and have dyecmd installed.)

## Legacy Server Importer

Thanks to the efforts from the Great Sage Of Keyboards, Andrei Akaras, we can
easily import to this server the data from TMW Legacy. Details can be obtained with
him. Please check the `hercules` folder for all scripts which carries over the data.

## Not Related

Many folders in this toolset are not relevant for this project, or used by other tools.
Please refrain from messing things! Thanks!
