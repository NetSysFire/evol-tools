#!/usr/bin/python2.7

# Setup
events=["Expo", "Fishing", "Kamelot", "Regnum",
        "Anniversary", "Christmas", "Easter", "Patrick", "Thanksgiving", "Valentine",
        "Worker"]

# Functions
def headers(val):
    return '\n\t<dialog name="aurora_%s" hideText="true">\n\t\t<menu>\n' % val

def navigation():
    nav=""
    nav+='\t\t\t<button x="300" y="20" name="Next" value="Ok" />\n'
    return nav

def tail():
    return '\t\t</menu>\n\t</dialog>\n'

def data(val):
    bf='\t\t\t<image x="0" y="0" image="graphics/images/aurora/%s.png" />\n' % val
    return bf

# Begin
f=open("aurora.tmp", "w")

f.write('<?xml version="1.0" encoding="utf-8"?>\n<!-- This file is generated automatically, editing it will have no effect.\n     Aurora Event Framework\n     (C) Jesusalva, 2020 -->\n<dialogs>')

for evtc in sorted(events):
    f.write(headers(evtc))
    f.write(data(evtc))
    f.write(navigation())
    f.write(tail())

f.write('\n</dialogs>')
f.close()

