#!/usr/bin/python2.7

# Setup
x=y=0
i=j=0

skills=[]

class Skill:
  def __init__(self, sid, name, icon, desc, bmp, amp, cmd, maxlv):
    self.id=sid
    self.lv=maxlv
    self.name=name
    self.icon=icon
    self.cmd=cmd

    self.desc=desc
    self.bmp=bmp
    self.amp=amp

def fillskill(sk, lv):
    sid=sk.id
    name=sk.name
    icon=sk.icon
    desc=sk.desc
    bmp=sk.bmp
    amp=sk.amp
    cmd=sk.cmd

    if lv > 0:
        lvstr='\t\t\tlevel="%d"\n' % lv
        mpstr='%d MP. ' % int(bmp+(amp*(lv-1)))
        cmdstr='\t\t\tinvokeCmd="@sk-%s"\n' % cmd
    else:
        lvstr=''
        lv=1
        mpstr=''
        cmdstr=''

    msg='\
\t\t<skill\n\
\t\t\tid="%d"\n\
\t\t\tname="%s"\n\
\t\t\ticon="graphics/skills/%s.png"\n\
\t\t\tdescription="%s%s"\n\
%s\
%s\
\t\t/>\n' % (sid, name, icon, mpstr, desc, cmdstr, lvstr)
    return msg











# Declare the skills

#########################
### Transmutation Skills
#########################
#skills.append(Skill(20024, "Parum", "other/parum", "Transmutate wood into stuff.",
#50, 0, "parum", 0))
#skills.append(Skill(20027, "Transmutation", "transmutation", "Transmute stuff into other stuff.",
#215, -5, "trans", 10))

#########################
### Summon Skills
#########################
skills.append(Skill(20025, "Summon Maggots", "other/kalmurk", "2x Maggot Slime.",
40, 5, "kalmurk", 0))
skills.append(Skill(20029, "Summon Dragon", "none", "4x Dragon Scale.",
50, 4, "dragokin", 0))
skills.append(Skill(20030, "Summon Slimes", "none", "20x Maggot Slime.",
30, 3, "limerizer", 0))
skills.append(Skill(20043, "Summon Fluffies", "none", "1x White Fur.",
25, 4, "cuteheart", 0))
skills.append(Skill(20042, "Summon Spiky", "none", "1x Mushroom Spores.",
25, 5, "kalspike", 0))
skills.append(Skill(20041, "Summon Mouboo", "none", "1x Mouboo Figurine.",
25, 5, "kalboo", 0))
skills.append(Skill(20036, "Summon Snakes", "none", "1x Snake Egg.",
35, 6, "halhiss", 0))
skills.append(Skill(20037, "Summon Wolverns", "none", "5x White Fur.",
45, 5, "kalwulf", 0))
skills.append(Skill(20038, "Summon Fairies", "none", "1x Fluo Powder.",
40, 4, "fairykingdom", 0))
skills.append(Skill(20039, "Summon Yetis", "none", "1x Frozen Yeti Tear.",
37, 5, "frozenheart", 0))
skills.append(Skill(20040, "Summon Terranite", "none", "1x Terranite Ore.",
47, 5, "stoneheart", 0))
skills.append(Skill(20044, "Summon Plants", "none", "2x Root.",
30, 3, "plantkingdom", 5))
skills.append(Skill(20047, "Summon Ducks", "none", "1x Cherry Cake. Req. Rubber Ducky.",
40, 7, "ducky", 0))
skills.append(Skill(20049, "Summon Pixies", "none", "3x Fluo Powder.",
40, 4, "fairyempire", 0))

skills.append(Skill(20023, "Summon Cave Maggot", "none", "Req. Zarkor Scroll.",
40, 7, "zarkor", 0))












# Begin
f=open("skills.tmp", "w")

f.write('<?xml version="1.0" encoding="utf-8"?>\n<!-- This file is generated automatically, editing it will have no effect.\n     (C) Jesusalva, 2019-2020 -->\n<skills>\n\t<set name="Summon">\n')

for sk in skills:
    i=0
    while (i < sk.lv):
        i+=1
        f.write(fillskill(sk, i))

    # Fill the fallback
    if (int(sk.lv)):
        sk.desc="MP + "+str(sk.amp)+"/lv. "+sk.desc
    f.write(fillskill(sk, -1))
    f.write("\n")

# We're done
f.write('\n\t</set>\n</skills>')
f.close()

