#! /usr/bin/env python3
# -*- coding: utf8 -*-
#
# Copyright (C) 2010-2011  Evol Online
# Copyright (C) 2018  TMW-2
# Author: Andrei Karas (4144)
# Author: Jesusalva

import datetime

wikia=open("Items.md", "w")
wikib=open("Monsters.md", "w")
wikic=open("../../client-data/x.diff", "w")

# the TYPEs we use to determine where to pack things
IT_HEALING=[]
IT_ETC=[]
IT_USABLE=[]
IT_AMMO=[]
IT_CARD=[]
IT_PETEGG=[]
IT_WEAPON={ 'HAND_2': [],           # TWO HAND (LR)
            'HAND_1':[]}            # WEAPONS (R)
IT_ARMOR={  'MISC': [],             # FOR FAILURE
            'EQP_ACC_L': [],        # ACCESSORY LEFT
            'EQP_ACC_R': [],        # ACCESSORT RIGHT
            'EQP_HEAD_MID': [],     # CHEST
            'EQP_SHOES': [],        # FEET
            'EQP_GARMENT': [],      # GLOVES
            'EQP_HEAD_LOW':[],      # PANTS
            '1024': [],             # NECKLACES (should be EQP_COSTUME_HEAD_TOP instead of number)
            '2048': [],             # RINGS (should be EQP_COSTUME_HEAD_MID instead of number)
            'EQP_HEAD_TOP':[],      # HATS/HELMETS
            'EQP_HAND_L': []}       # SHIELDS

Mobs1=[]
Mobs2=[]
Mobs3=[]
Mobs4=[]
Mobs5=[]
MobsA=[]

SysDrops=[]


def printSeparator():
    print("--------------------------------------------------------------------------------")

def showHeader():
    print("TMW2 Wiki Generator")
    ##print "Evol client data validator."
    print("Run at: " + datetime.datetime.now().isoformat())
    ##print "https://gitlab.com/evol/evol-tools/blob/master/testxml/testxml.py"
    printSeparator()

def showFooter():
    #pass
    #printSeparator()
    print("Done.")





def fwalk(wmask):
    if wmask == 'WATER':
        return '<font color=#00f>%s</font>' % (wmask)
    elif wmask == 'AIR':
        return '<font color=#093>%s</font>' % (wmask)
    elif wmask == 'WALL':
        return '<font color=#f00>%s</font>' % (wmask)
    elif wmask == 'NORMAL' or wmask == 'DEFAULT':
        return '<font color=#111>%s</font>' % (wmask)
    else:
        print("Invalid walk mask: "+wmask)
        exit(1)

def WhatRace(rac):
    rc=rac.race
    if rc == 0:
        return "Formless"
    elif rc == 1:
        return "Undead"
    elif rc == 2:
        return "Brute"
    elif rc == 3:
        return "Plant"
    elif rc == 4:
        return "Insect"
    elif rc == 5:
        return "Fish"
    elif rc == 6:
        return "-"
    elif rc == 7:
        return "SemiHuman"
    elif rc == 8:
        return "Angel"
    elif rc == 9:
        return "Dragon"
    elif rc == 10:
        return "Player"
    elif rc == 11:
        return "Boss"
    elif rc == 12:
        return "NonBoss"
    elif rc == 14:
        return "NonSemiHuman"
    elif rc == 15:
        return "NonPlayer"
    elif rc == 16:
        return "SemiPlayer"
    elif rc == 17:
        return "NonSemiPlayer"
    else:
        print("ERROR, INVALID RACE ID: %d (ID: %s)" % (rc, rac.id))
        exit(1)

def WhatElem(rac):
    rc=rac.elem
    tl="ERROR"
    cl="#F00"
    if rc == 0:
        tl,cl="Neutral","#000"
    elif rc == 1:
        tl,cl="Water","#00F"
    elif rc == 2:
        tl,cl="Earth","#7A0"
    elif rc == 3:
        tl,cl="Fire","#F00"
    elif rc == 4:
        tl,cl="Wind","#093"
    elif rc == 5:
        tl,cl="Poison","#040"
    elif rc == 6:
        tl,cl="Holy","#afa"
    elif rc == 7:
        tl,cl="Dark","#908"
    elif rc == 8:
        tl,cl="Ghost","#404"
    elif rc == 9:
        tl,cl="Undead","#440"
    else:
        print("ERROR, INVALID ELEM ID: %d (ID: %s)" % (rc, rac.id))
        exit(1)
    return "<font color=%s>%s</font>" % (cl, tl)

class Mob:
  def __init__(self):
    # Basic
    self.id="0"
    #self.aegis="UnknownMonster" # SpriteName is not used anywhere, we are using its ID
    self.name="Unknown Monster Name"
    self.view="1"

    # Defensive
    self.mobpt="0" # Mob Points “Level”
    self.hp="0"
    self.xp="0"
    self.jp="0"
    self.st=""

    # Offensive
    self.atk="[0, 0]"
    self.range="0"
    self.move="0"
    self.delay="0"
    self.drops=[]

    # Elegen Info
    self.race=-1
    self.elem=-1
    self.elel=-1
    self.walk="NORMAL"

def MobAlloc(ab):
    try:
        maab=int(ab.mobpt)
    except:
        maab=9901

    if maab <= 20:
        Mobs1.append(ab)
    elif maab <= 40:
        Mobs2.append(ab)
    elif maab <= 60:
        Mobs3.append(ab)
    elif maab <= 80:
        Mobs4.append(ab)
    elif maab <= 100:
        Mobs5.append(ab)
    else:
        MobsA.append(ab)

def testMobs():
    print("Generating Mob Wiki...")
    src=open("../../server-data/db/re/mob_db.conf", "r")
    wikib.write("<h1 id='monster-database'>Monster Database</h1>\n")
    start=False
    dropper=False
    x=Mob() # Only for pyflakes2

    for a in src:
        if a == "{\n":
            if start:
                MobAlloc(x)
            else:
                start=True
            x=Mob()

        if "	Id:" in a:
            x.id=stp(a)
        if x.id == "ID":
            continue

        if "	Name:" in a:
            x.name=stp(a)
        elif "	Hp:" in a:
            x.hp=stp(a)
        elif "	Lv:" in a:
            x.mobpt=stp(a)
        elif "	Exp:" in a:
            x.xp=stp(a)
        elif "	JExp:" in a:
            x.jp=stp(a)
        elif "	Attack:" in a:
            x.atk=stp(a)
        elif "	AttackRange:" in a:
            x.range=stp(a)
        elif "	MoveSpeed:" in a:
            x.move=stp(a)
        elif "	ViewRange:" in a:
            x.view=stp(a)
        elif "	AttackDelay:" in a:
            x.delay=stp(a)
        elif "	Looter: true" in a:
            x.st+="<font color=#790>Lot</font>,"
        elif "	Assist: true" in a:
            x.st+="<font color=#0a0>Ass</font>,"
        elif "	Aggressive: true" in a:
            x.st+="<font color=#f00>Agr</font>,"
        elif "	WalkMask:" in a:
            x.walk=stp(a)
        elif "	Element:" in a:
            tmp=stp(a)
            tmp2=tmp.split(',')
            try:
                x.elem=int(tmp2[0])
                x.elel=int(tmp2[1])
            except:
                print("Invalid Element for mob %s: %s" % (x.id, tmp))
                exit(1)
        elif "	Race:" in a:
            try:
                x.race=int(stp(a))
            except:
                print("Invalid Race for mob %s: %s" % (x.id, a))
                exit(1)
        elif 'Drops: ' in a:
            dropper=True
        elif dropper and '}' in a:
            dropper=False
        elif dropper:
            x.drops.append(stp(a).split(": "))
    # Write last entry
    MobAlloc(x)

    writeMob()

    wikib.write('\n\n|Mode|Desc|\n|----|----|\n')
    wikib.write('|Lot|Looter|\n')
    wikib.write('|Ass|Assist|\n')
    wikib.write('|Agr|Aggressive|\n')

    src.close()

def stp(x):
    return x.replace('\n', '').replace('|', '').replace('(int, defaults to ', '').replace(')', '').replace('basic experience', '').replace('"','').replace("    ","").replace("\t","").replace('(string', '').replace('Name: ','').replace('Element: ','').replace('Race: ','').replace('AttackDelay: ', '').replace('WalkMask: ','').replace('MoveSpeed: ', '').replace('AttackRange: ', '').replace('ViewRange: ','').replace('Attack: ','').replace('ViewRange: ','').replace('Hp: ','').replace('Id: ','').replace('Lv: ','').replace('view range','').replace('attack range','').replace('move speed','').replace('health','').replace('(int','').replace('attack delay','atk.').replace('(','').replace(')','').replace('WALK_','')


def MonsterWrite(tbl):
    # TODO: Check _mobs files to determine the usual monster density (a misc info to aid adding proper drop specs)
    wikib.write("<table border=1>\n")
    wikib.write("<tr><th>ID</th><th>Name</th><th>Mob Info</th><th>Elegen</th><th>Misc Info</th><th>Rewards</th><th>Drops</th></tr>\n")
    for i in tbl:
        if i.id == 'ID':
            continue
        wikib.write('<tr><td><a name="' + i.id + '"></a>' +
                    i.id +"</td><td>"+
                    i.name +"</td><td>"+
                    mb_core(i) +"</td><td>"+
                    mb_eleg(i) +"</td><td>"+
                    mbdt('misc', mb_rdmisc(i)) +"</td><td>"+
                    mbdt('Exp\'s', mb_rdrw(i)) +"</td><td>"+
                    mbdt('drops', mb_rddrop(i)) +"</td></tr>\n"
                    )
    wikib.write("</table>\n")
    wikib.write("\n<a href=#monster-database>(↑) Return to top</a>\n\n")

def writeMob():
    wikib.write("\
+ [Level 0-20](#starter)\n\
+ [Level 21-40](#apprentice)\n\
+ [Level 41-60](#intermediary)\n\
+ [Level 61-80](#advanced)\n\
+ [Level 81-100](#expert)\n\
+ [Level 100+](#out-of-scope)\n\n\
    ")

    wikib.write("## Starter\n\n")
    MonsterWrite(Mobs1)

    wikib.write("## Apprentice\n\n")
    MonsterWrite(Mobs2)

    wikib.write("## Intermediary\n\n")
    MonsterWrite(Mobs3)

    wikib.write("## Advanced\n\n")
    MonsterWrite(Mobs4)

    wikib.write("## Expert\n\n")
    MonsterWrite(Mobs5)

    wikib.write("## Out Of Scope\n\n")
    MonsterWrite(MobsA)


def mbdt(summary, content):
    return "<details>\
<summary>"+summary+"</summary>\
<pre>"+content+"</pre></details>"

def mb_core(mb):
    buff=""
    buff+="Lvl: %s<br/>\n" % (mb.mobpt)
    buff+="HP: <b><font color=#0a0>%s</font></b><br/>\n" % (mb.hp)
    buff+="ATK: <b><font color=#a00>%s</font></b><br/>\n" % (mb.atk)
    if mb.st != "":
        buff+="Modes: <i>%s</i>" % (mb.st)
    return buff

def mb_eleg(mb):
    buff=""
    buff+="Race: %s<br/>\n" % (WhatRace(mb))
    buff+="Walk: %s<br/>\n" % (fwalk(mb.walk))
    buff+="Element: %s<br/>\n" % (WhatElem(mb))
    return buff

def mb_rdmisc(mb):
    buff=""
    if "agr" in mb.st.lower():
        buff+="View Range: %s\n" % (mb.view)
    buff+="Attack Range: %s\n" % (mb.range)
    buff+="AtkDelay: %s ms\n" % (mb.delay)
    buff+="Move speed: %s ms\n" % (mb.move)
    buff+="Element Level: %d\n" % (mb.elel)
    return buff

def mb_rdrw(mb):
    buff=""
    try:
        buff+="MobPoints: %d\n" % (int(mb.mobpt)*11/10)
    except:
        pass
    buff+="%s\n" % (mb.xp)
    buff+="%s\n" % (mb.jp)
    return buff

def mb_rddrop(mb):
    buff=""
    for ax in mb.drops:
        # Ignore disabled drops
        if ax[0].startswith("//"):
            continue

        # Write drop
        try:
            buff+=ax[0]+': ' + str(int(ax[1])/100.0) + ' %\n'
        except IndexError:
            print("Fatal: invalid %s mob with %s drops" % (mb.name, str(ax)))
            exit(1)
        except:
            print("[Warning] %s incorrect drop: %s" % (mb.name, str(ax)))
            buff+=ax[0]+': ' + ax[1] + ' ppm\n'

        # Save to SysDrops
        SysDrops.append([ax[0], ax[1], mb.name])

    return buff


class It:
  def __init__(self):
    # Basic
    self.id="0"
    self.aegis="UnknownItem"
    self.name="Unknown Item Name"
    self.price="0" # Sell price, of course
    self.weight="0"
    self.type="IT_ETC" # default type
    self.loc=""

    # Offensive/Defensive
    self.atk="0"
    self.matk="0"
    self.range="0"
    self.defs="0"

    # Restrictions (EquipLv)
    self.lvl="0"
    self.drop=True
    self.trade=True
    self.sell=True
    self.store=True

    # Special settings
    self.rare=False         # DropAnnounce
    self.script=False

    # Visual
    self.sl="0" # Slots
    self.ac=False # Allow Cards

    # Script settings
    self.minheal="0"
    self.maxheal="0"
    self.delheal="0"

def ItAlloc(it):
    if (it.sl == "0" and it.ac) or (it.sl in ["1","2","3","4"] and not it.ac):
        print("WARNING, item id "+it.id+" invalid dye/card setting!")
    if (len(it.sl) > 1):
        print("WARNING, item id "+it.id+" bad slots length: %d (%s)" % (len(it.sl), it.sl))
    if it.ac:
        wikic.write(it.id + ": " + it.name + "\n")

    a=it.type
    if "IT_HEALING" in a:
        IT_HEALING.append(it)
    elif "IT_ETC" in a:
        IT_ETC.append(it)
    elif "IT_USABLE" in a:
        IT_USABLE.append(it)
    elif "IT_AMMO" in a:
        IT_AMMO.append(it)
    elif "IT_CARD" in a:
        IT_CARD.append(it)
    elif "IT_PETEGG" in a:
        IT_PETEGG.append(it)

    elif "IT_WEAPON" in a:
        if "HAND_L" in it.loc or "EQP_ARMS" in it.loc:
            IT_WEAPON["HAND_2"].append(it)
        elif "HAND_R" in it.loc:
            IT_WEAPON["HAND_1"].append(it)
        else:
            raise Exception("Invalid location for weapon: %s" % it.loc)

    elif "IT_ARMOR" in a:
        if 'EQP_ACC_L' in it.loc:
            IT_ARMOR['EQP_ACC_L'].append(it)
        elif 'EQP_ACC_R' in it.loc:
            IT_ARMOR['EQP_ACC_R'].append(it)
        elif 'EQP_HEAD_MID' in it.loc:
            IT_ARMOR['EQP_HEAD_MID'].append(it)
        elif 'EQP_SHOES' in it.loc:
            IT_ARMOR['EQP_SHOES'].append(it)
        elif 'EQP_GARMENT' in it.loc:
            IT_ARMOR['EQP_GARMENT'].append(it)
        elif 'EQP_HEAD_LOW' in it.loc:
            IT_ARMOR['EQP_HEAD_LOW'].append(it)
        elif 'EQP_HEAD_TOP' in it.loc:
            IT_ARMOR['EQP_HEAD_TOP'].append(it)
        elif 'EQP_HAND_L' in it.loc:
            IT_ARMOR['EQP_HAND_L'].append(it)
        elif '1024' in it.loc:
            IT_ARMOR['1024'].append(it)
        elif '2048' in it.loc:
            IT_ARMOR['2048'].append(it)
        elif 'EQP_SHADOW_ACC_R' in it.loc:
            IT_ARMOR['EQP_ACC_R'].append(it)
        else:
            raise Exception("Invalid Loc for ID %s: %s" % (it.id, it.loc))

def newItemDB():
    print("Generating Item Wiki...")
    src=open("../../server-data/db/re/item_db.conf", "r")

    x=It()
    for a in src:
        if a == "{\n":
            ItAlloc(x)
            x=It()

        # sti() block
        if "	Id:" in a:
            x.id=sti(a)
        elif "	AegisName:" in a:
            x.aegis=sti(a)
        elif "	Name:" in a:
            x.name=stin(a)
        elif "	Sell:" in a:
            x.price=sti(a)
        elif "	Weight:" in a:
            x.weight=sti(a)
        elif "	Type:" in a:
            x.type=sti(a)
        elif "	Loc:" in a:
            x.loc=sti(a)
        elif "	Atk:" in a:
            x.atk=sti(a)
        elif "	Matk:" in a:
            x.matk=sti(a)
        elif "	Range:" in a:
            x.range=sti(a)
        elif "	Def:" in a:
            x.defs=sti(a)
        elif "	EquipLv:" in a:
            x.lvl=sti(a)
        elif "	Slots:" in a:
            x.sl=sti(a)
        elif "	AllowCards:" in a:
            x.ac=True
        # Write booleans
        elif "DropAnnounce: true" in a:
            x.rare=True
        elif "nodrop: true" in a:
            x.drop=False
        elif "notrade: true" in a:
            x.trade=False
        elif "noselltonpc: true" in a:
            x.sell=False
        elif "nostorage: true" in a:
            x.store=False
        elif "Script" in a:
            x.script=True
        # For healing items
        elif "@min" in a:
            x.minheal=sti(a)
        elif "@max" in a:
            x.maxheal=sti(a)
        elif "@delay" in a:
            x.delheal=sti(a)

    # Write last entry
    ItAlloc(x)
    writeItems()

    src.close()

def sti(x):
    return x.replace('\n', '').replace('|', '').replace(')', '').replace('Id: ', '').replace('"','').replace("    ","").replace("\t","").replace('AegisName: ', '').replace('Name: ','').replace('Sell: ', '').replace('Weight: ', '').replace('Type: ', '').replace('Loc: ', '').replace('Atk: ', '').replace('Matk: ', '').replace('Range: ', '').replace('Def: ', '').replace('EquipLv: ', '').replace('Slots: ','').replace(" ", "").replace('@min=','').replace('@max=','').replace('@delay=','').replace(';','')

def stin(x):
    return x.replace('\n', '').replace('|', '').replace(')', '').replace('Id: ', '').replace('"','').replace("    ","").replace("\t","").replace('Name: ','').replace(';','')


def writeItems():
    wikia.write("# Items\n\
+ [Healing Items](#healing-items)\n\
+ [Usable Items](#usable-items)\n\
+ [Generic Items](#generic-items)\n\
+ [Ammo](#ammo)\n\
+ [Cards](#cards)\n\
+ [Pet Eggs](#pet-eggs)\n\
+ [Weapons](#weapons)\n\
  + [1H Weapons](#1h-weapons)\n\
  + [2H Weapons](#2h-weapons)\n\
+ [Armors](#armors)\n\
  + [Left Accessory](#left-accessory)\n\
  + [Right Accessory](#right-accessory)\n\
  + [Headgear](#headgear)\n\
  + [Chest](#chest)\n\
  + [Pants](#pants)\n\
  + [Shoes](#shoes)\n\
  + [Necklaces](#necklaces)\n\
  + [Rings](#rings)\n\
  + [Gloves](#gloves)\n\
  + [Shields](#shields)\n\
\n\n")
    wikia.write("#### Restrictions Reference\n")
    wikia.write("Special Aegis Name Markers:\n\
+ * - Rare item with drop announce.\n\
+ (dp) - This item cannot be dropped.\n\
+ (tr) - This item cannot de traded.\n\
+ (sl) - This item cannot be sold.\n\
+ (gg) - This item cannot go to storage.\n\n")

    # Healing Items
    wikia.write("## Healing Items\n\n")
    ItemWrite(IT_HEALING, ID=True, AEGIS=True, PRICE=True, WEIGHT=True, HEALING=True, DROPPER=True)

    # Usable Items
    wikia.write("## Usable Items\n")
    ItemWrite(IT_USABLE, ID=True, AEGIS=True, NAME=True, PRICE=True, WEIGHT=True, DROPPER=True)

    # Generic Items
    wikia.write("## Generic Items\n")
    ItemWrite(IT_ETC, ID=True, AEGIS=True, NAME=True, PRICE=True, WEIGHT=True, DROPPER=True)

    # Ammo Items
    wikia.write("## Ammo\n")
    ItemWrite(IT_AMMO, ID=True, AEGIS=True, NAME=True, WEIGHT=True, ATK=True)

    # Card Items
    wikia.write("## Cards\n")
    ItemWrite(IT_CARD, ID=True, AEGIS=True, NAME=True, PRICE=True, WEIGHT=True)

    # Pet Egg Items
    wikia.write("## Pet Eggs\n")
    ItemWrite(IT_PETEGG, ID=True, AEGIS=True, NAME=True, WEIGHT=True)

    ####################################################################
    wikia.write("# Weapons\n")

    # 1 Hand Items
    wikia.write("## 1H Weapons\n")
    ItemWrite(IT_WEAPON['HAND_1'], ID=True, AEGIS=True, PRICE=True, WEIGHT=True, ATK=True, LVL=True, DROPPER=True)

    # 2 Hand Items
    wikia.write("## 2H Weapons\n")
    ItemWrite(IT_WEAPON['HAND_2'], ID=True, AEGIS=True, PRICE=True, WEIGHT=True, ATK=True, LVL=True, RANGE=True)


    ####################################################################
    wikia.write("# Armors\n")

    ArmorWrite("Left Accessory",'EQP_ACC_L')
    ArmorWrite("Right Accessory",'EQP_ACC_R')
    ArmorWrite("Headgear",'EQP_HEAD_TOP')
    ArmorWrite("Chest",'EQP_HEAD_MID')
    ArmorWrite("Pants",'EQP_HEAD_LOW')
    ArmorWrite("Shoes",'EQP_SHOES')
    ArmorWrite("Necklaces",'1024')
    ArmorWrite("Rings",'2048')
    ArmorWrite("Gloves",'EQP_GARMENT')
    ArmorWrite("Shields",'EQP_HAND_L')

# Write AegisName with restrictions
def hl(it):
    buff=""
    if it.rare:
        buff+="*"
    buff+=it.aegis
    buff+=" "
    if not it.drop:
        buff+="<a href='#restrictions-reference'>(dp)</a>"
    if not it.trade:
        buff+="<a href='#restrictions-reference'>(tr)</a>"
    if not it.sell:
        buff+="<a href='#restrictions-reference'>(sl)</a>"
    if not it.store:
        buff+="<a href='#restrictions-reference'>(gg)</a>"
    return buff

#    wikia.write("Id|Aegis|Name|Weight|Atk|Matk|\n")
#    wikia.write("Id|Aegis|Name|Price|Weight|\n")

def ItemWrite(tbl, ID=False, AEGIS=False, NAME=False, PRICE=False, WEIGHT=False, DEF=False, LVL=False, ATK=False, RANGE=False, HEALING=False, SCRIPT=False, DROPPER=False):
    wikia.write("<table border=1>\n")
    wikia.write("<tr>")
    if ID:
        wikia.write("<th>ID</th>")
    if AEGIS:
        wikia.write("<th>Aegis</th>")
    if NAME:
        wikia.write("<th>Name</th>")
    if PRICE:
        wikia.write("<th>Price</th>")
    if WEIGHT:
        wikia.write("<th>Weight</th>")
    if DEF:
        wikia.write("<th>Def</th>")
    if LVL:
        wikia.write("<th>Lvl</th>")
    if ATK:
        wikia.write("<th>Atk</th>")
        wikia.write("<th>Matk</th>")
    if RANGE:
        wikia.write("<th>Range</th>")
    if HEALING:
        wikia.write("<th>Min</th>")
        wikia.write("<th>Max</th>")
        wikia.write("<th>Delay</th>")
    if SCRIPT:
        wikia.write("<th>Script</th>")
    if DROPPER:
        wikia.write("<th>Mobs</th>")

    wikia.write("</tr>\n")

    for i in tbl:
        wikia.write('<tr>')

        if ID:
            wikia.write("<td><a name=\"%s\"></a>%s</td>" % (i.id,i.id))
        if AEGIS:
            wikia.write("<td>%s</td>" % hl(i))
        if NAME:
            wikia.write("<td>%s</td>" % i.name)
        if PRICE:
            wikia.write("<td>%s GP</td>" % i.price)
        if WEIGHT:
            wikia.write("<td>%s g</td>" % i.weight)
        if DEF:
            wikia.write("<td>Def: %s</td>" % i.defs)
        if LVL:
            wikia.write("<td>Lv: %s</td>" % i.lvl)
        if ATK:
            wikia.write("<td>Atk: %s</td>" % i.atk)
            wikia.write("<td>%s</td>" % i.matk)
        if RANGE:
            wikia.write("<td>%s</td>" % i.range)
        if HEALING:
            wikia.write("<td>%s</td>" % i.minheal)
            wikia.write("<td>%s</td>" % i.maxheal)
            wikia.write("<td>%s s</td>" % i.delheal)
        if SCRIPT:
            wikia.write("<td>%s</td>" % i.script)
        # TODO: Check for item Aegis in npc/ folder too, to determine shops and quests.
        if DROPPER:
            tmp_droppers=""
            tmp_drpalign=[]
            for ax in SysDrops:
                if ax[0] == i.aegis:
                    tmp_drpalign.append([ax[2], ax[1]])
            if len(tmp_drpalign) > 0:
                for a in sorted(tmp_drpalign,  key=lambda xcv: xcv[1], reverse=True):
                    try:
                        ppm=int(a[1])/100.0
                        tmp_droppers+=("%s: %.2f %% \n" % (a[0], ppm))
                    except:
                        print("[Warning] %s whodrop error: %s" % (i.name, str(a)))
                wikia.write("<td>%s</td>" % mbdt("monsters", tmp_droppers))
            else:
                wikia.write("<td>-</td>")

        wikia.write("</tr>")

    wikia.write("</table>\n")
    wikia.write("\n[(↑) Return to top](#items)\n\n")

def ArmorWrite(name,scope):
    wikia.write("## "+name+"\n")
    ItemWrite(IT_ARMOR[scope], ID=True, AEGIS=True, PRICE=True, WEIGHT=True, DEF=True, LVL=True, SCRIPT=True)

showHeader()

testMobs()
newItemDB()

wikia.close()
wikib.close()
wikic.close()
#print(str(SysDrops))

showFooter()
exit(0)
