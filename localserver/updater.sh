#!/usr/bin/env bash

# Apply beta.patch if it exists to server-data
# It will only exist on BETA SERVERS, though
if [ -e "./beta.patch" ]
  then
    echo "Apply beta.patch ........"
    cd ../../server-data
    git apply ../tools/localserver/beta.patch
    cd $DIR
    mv beta.patch .~beta.patch
    ls
    echo "........ Done."
fi

# Apply beta.patch2 if it exists to server-code
# It will only exist on BETA SERVERS, though
if [ -e "./beta.patch2" ]
  then
    echo "Apply server updates ........"
    sleep 1
    cd ../../server-code
    git checkout -- src
    echo "Rolling server back to correct version"
    #git diff master ce2dbb6acdc559ec256d1f9f9a779b8283064708 > x.diff
    #ls
    #head -n 25 x.diff
    #tail -n 40 x.diff
    #git apply --reject --whitespace=nowarn x.diff
    git status
    ls --recursive|grep ".rej"
    cd ../tools/localserver
    echo "Server code clean ........"
    sleep 1
    python applicator.py
    echo "Apply beta.patch2 ........"
    cd ../../server-code
    git apply ../tools/localserver/beta.patch2
    cd $DIR
    #mv beta.patch2 .~beta.patch2
    ls
    echo "........ Done."
fi

