#!/usr/bin/env bash

export SD="../../server-data"
export CONF="$SD/conf/import"
export NPC="$SD/npc"

mkdir $CONF
cp conf/* $CONF
cp -f ${SD}/conf/channels.conf.base ${SD}/conf/channels.conf
cp -f npc/017-1_* ${NPC}/017-1/
cp -f npc/015-8_* ${NPC}/015-8/
cp -f npc/025-2-4_* ${NPC}/025-2-4/
cp -f npc/018-7-1_* ${NPC}/018-7-1/
cp -f npc/botcheck_* ${NPC}/botcheck/
cp -f npc/easteregg_* ${NPC}/config/
mkdir versions
echo "4" >versions/confver
